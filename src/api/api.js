// 封装API接口
import axios from 'axios'
// 全局地址
axios.defaults.baseURL = "http://127.0.0.1:5000/"

// 1.获取商家数据
export var shopseller = () => axios.get('/shop/seller');
//2.获取商品数据 
export var goodslist = () => axios.get('/goods/goods_list');
// 3.获取评价数据
export var shopratings = () => axios.get('/shop/ratings')